#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import scipy 
import scipy.signal 
import matplotlib.pyplot as plt
import re
from ReadASCIIFile import ReadASCIIFile

# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0
import scipy.linalg as la
#import numpy as np

def expfit(deg,x1,h,y):
    #Build matrix
    A=la.hankel(y[:-deg],y[-deg-1:])
    a=-A[:,:deg]    
    b= A[:,deg]
    #Solve it
    s=la.lstsq(a,b)[0]
    #Solve polynomial
    p=np.flipud(np.hstack((s,1)))
    u=np.roots(p)
    #Calc exponential factors
    a=np.log(u)/h
    #Build power matrix
    A=np.power((np.ones((len(y),1))*u[:,None].T),np.arange(len(y))[:,None]*np.ones((1,deg)))
    #solve it 
    f=la.lstsq(A,y)[0]
    #calc amplitudes 
    c=f/np.exp(a*x1)    
    #build x, approx and calc rms
    x=x1+h*np.arange(len(y))
    approx=np.exp(x[:,None]*a[:,None].T).dot(c[:,None])
    rms=np.sqrt(((approx-y)**2).sum())
    return a, c,rms

                        
def spectrum(t,x,kind='fft',freq=np.array([1.0]),window=0,
             norm_in='none',norm_out=False,
             maxima=False,showfreq=False) :
    ''' The spectrum function compute the fourier trasform of a signal t,x(t) and 
    is called as h=spectrum(t,x).  By default it compute the standard "fft" of 
    the signal.

    OPTIONS: kind    = 'fft','int'',lomb'      [default=fft]
             norm_in = 'none','mean','linfit'  [subtract to x the mean or the linear term]
             freq    = np.array([...]) frequncy to use to calculate the 'lomb'
                       or the integral trasformation.
             window  = 0,1,2,3,4 [windowing function 0=none,1=blackman,2=bartlett
                                  3=hamming,4=hanning]
    FLAGS:   norm_out = 'False' [normalize the output to be max(abs(h))=1]
             maxima   = 'False' [Output the ordered maximum of (f,h)]
             showfreq = 'False' [Output (f,h) instead of just h]
    '''
    #####################################
    ## Take out the mean value from FFT
    #####################################
    if (norm_in=='mean') :
       signal = x/np.mean(x)
    if (norm_in=='linfit') :
       poly =np.polyfit(t,x,1)
       poly1=np.poly1d(poly)
       signal = x -poly1(t) 
    else :
       signal = np.array(x)

    LEN = len(signal)
    ####################################
    ## Windowing function:
    ## (0)  None
    ## (1)  np.blackman
    ## (2)  np.bartlett
    ## (3)  np.hamming
    ## (4)  np.hanning
    ####################################
    if   (window == 1 ) :  signal*=scipy.signal.blackman(LEN)
    elif (window == 2 ) :  signal*=scipy.signal.bartlett(LEN)
    elif (window == 3 ) :  signal*=scipy.signal.hamming(LEN)
    elif (window == 4 ) :  signal*=scipy.signal.hanning(LEN)

    if kind == 'fft' :
        dt = t[1] -t[0]
        T  = t[-1]-t[0]
        f = 1/T * np.arange(0,LEN,dtype=np.float64)
        h = (T/LEN)* scipy.fft(signal)
    elif kind == 'lomb':
        f=freq
        if np.any(np.iscomplex(signal)) :
            h=scipy.signal.lombscargle(t,np.real(signal),freq)
        else:
            h=scipy.signal.lombscargle(t,signal,freq)
    elif kind == 'int':
        f=freq
        h=np.array([np.trapz(signal*np.exp(-1j*2*np.pi*ff*t),t) for ff in freq])

    if norm_out== True :
        h=h/np.max(np.abs(h))
    if maxima == True :
        data = abs(h)
        c    = (np.diff(np.sign(np.diff(data))) < 0).nonzero()[0] + 1 # local max
        idx  = np.argsort(abs(h[c]))
        return (f[c[idx[::-1]]],h[c[idx[::-1]]])
    elif showfreq == False :
        return h
    else:        
        return (f,h)

################################################################
####
################################################################

def trialB_DEF(p):
    return {'const': p[0]
           ,'linear': p[1]
           ,'A'    : p[2] 
           ,'f'    : p[3]
           ,'phi'  : p[4]
           }
def trialB_curve(x,*p) :
    return trialB(x,p)
def trialB(x,p) :
    return p[0] + x * p[1] + p[2] * np.sin( 2*np.pi*p[3]*x + p[4])
def trialB_JAC(p,x,y) :
    return [ 
      np.ones(len(x))
    , x 
    , np.sin( 2*np.pi*p[3]*x + p[4])
    , p[2] * 2  *np.pi * x *np.cos( 2*np.pi*p[3]*x + p[4])
    , p[2] * np.cos( 2*np.pi*p[3]*x + p[4])
     ]
def trialB_RES(p, x, y):
    return trialB(x,p) - y

################################################################
####
################################################################

def trial_DEF(p):
    return {'1/tau': p[1]
           ,'tau'  : 1.0/p[1]
           ,'f'    : p[2]
           ,'phi'  : p[3]
           ,'A'    : p[0] 
           }
def trial_COS(x,p) :
    return p[0] * np.exp(p[1]*x) * np.cos( 2*np.pi*p[2]*x + p[3] )
def trial_curve(x,*p) :
    return trial(x,p)
def trial(x,p) :
    return p[0] * np.exp(p[1]*x) * np.sin( 2*np.pi*p[2]*x + p[3] )

def trial_JAC(p,x,y) :
    return [ 
      np.exp(p[1]*x) * np.sin( 2*np.pi*p[2]*x + p[3])
    , x * (p[0] * np.exp(p[1]*x) * np.sin( 2*np.pi*p[2]*x + p[3]) )
    , 2*np.pi*x * (p[0] * np.exp(p[1]*x) * np.cos( 2*np.pi*p[2]*x + p[3]) )
    , (p[0] * np.exp(p[1]*x) * np.cos( 2*np.pi*p[2]*x + p[3]) )
     ]

def trial_RES(p, x, y):
    return trial(x,p) - y

################################################################
####
################################################################

def trialM_DEF(p):
    return {'1/tau': p[1]
           ,'tau'  : 1.0/p[1]
           ,'A'    : p[0] 
           }

def trialM_curve(x,*p) :
    return trialM(x,p)
def trialM(x,p) :
    return p[0] * np.exp(p[1]*x) 
def trialM_JAC(p,x,y) :
    return [ 
      np.exp(p[0]*x) 
    , p[0] * np.exp(p[1]*x) 
     ]

def trialM_RES(p, x, y):
    return trialM(x,p) - y

################################################################
####
################################################################
#
def NonLinearFit_ComplexExpGrow(x_IN,y_IN,interval=[],guess=[]
              ,kind=0,full_output=False,) :
    ''' Non linear fit of the signal y(x) in the interval=[x_i,x_f]
    using the guess function:
        p[0] * exp( p[1]*(x-x[0]) ) * sin(2 pi p[2] (x-x[0])+ p[3] )
    '''
    if ( len(interval) == 2) :        
        IDX = plt.mlab.find(np.logical_and( x_IN >= interval[0],x_IN <= interval[1]))
        x = x_IN[IDX]
        y = y_IN[IDX]
    else:
        x = x_IN
        y = y_IN

    if np.any(np.iscomplex(y)) :
        y = np.real(y)
        y_is_complex = True
    else:
        y_is_complex = False
    if (len(guess)!=4) :
        guess=[np.mean(y),0.0,0.0,0.0]
    try:
        fit, pcov,info,msg,ier = scipy.optimize.leastsq(
                trial_RES,guess,args=(x-x[0],y)
               ,full_output=True
               ,maxfev=10000, ftol=1.e-5, xtol=1.e-8
               ,Dfun=trial_JAC,col_deriv=True
               )
    except:
        ier = 5
        msg = "Non-Linear fit error ! returning guess!"
        print msg
        fit = guess 
    # calculate final chi square

    print fit
    chisq=sum(info["fvec"]*info["fvec"])
    dof=len(x)-len(fit)
    # chisq, sqrt(chisq/dof) agrees with gnuplot
    # uncertainties are calculated as per gnuplot, "fixing" the result
    # for non unit values of the reduced chisq.
    # values at min match gnuplot
    print 'y=p[0] * exp( p[1]*(x-x[0]) ) * sin(2 pi p[2] (x-x[0])+ p[3] )'
    print "Fitted parameters at minimum, with 68% C.I.:"
    for i,param in enumerate(fit):
        print "%2i %-10s %12f +/- %10f (%10f)"% (
               i,'p[%d]'%i,param
              ,np.sqrt(pcov[i,i])*np.sqrt(chisq/dof)
              ,np.sqrt(pcov[i,i])
               )
    print

  
    #################################
    #### Output Fit Results
    #################################
    if (y_is_complex == True ) :
        y_fit = trial(x-x[0],fit) - 1j*trial_COS(x-x[0],fit)
    else :
        y_fit = trial(x-x[0],fit)

    if  (full_output == False) :
        return fit
    else :
        if ((ier >= 1) and (ier<=4) ):
             return (fit, { 'x' : x
                          , 'y' : y
                      ,'y_fit' : y_fit
                      , 'fit'  : fit
                      , 'err'  : np.array([np.sqrt(pcov[i,i]) for i in range(len(fit))])
                      ,'pcov'  : pcov
                      ,'info'  : info
                      ,'msg'   : msg
                      ,'ier'   : ier
                   })
        else:
             return (fit, { 'x' : x
                          , 'y' : y
                      ,'y_fit' : y_fit
                      , 'fit'  : fit
                      ,'info'  : info
                      ,'msg'   : msg
                      ,'ier'   : ier
                   })

################################################################
####
################################################################


def NonLinearFit(x_IN,y_IN,interval=[],kind=0,out_fit=False,full_output=False,guess=[]) :
    '''Non linear fit of the signal y(x). 

    If inteval is specified the values used are: interval(0) <= x <= interval(1)

    The supported kind of fit  are:
       kind=0 : p[0] +p[1]*x + p[2] * sin(2 pi p[3] + p[4] ) 
       kind=1 : p[0] * exp(p[1]*x) 
       kind=2 : p[0] * exp(p[1]*x) * sin(2 pi p[2] + p[3] )
    NOTE kind=1,2 can deal with complex or real y. In Case of compex y the fit
                  is done "kind=1" on y=abs(y_IN) or "kind=2" on  y=real(y_IN)  
    '''

    if ( len(interval) == 1) :
        if (interval[0]==-1) :
            m2=np.abs(y_IN)
            m2MAX=np.max(m2)
            IDX_0=plt.mlab.find(m2 >= 0.05 * m2MAX)[0]
            IDX_1=plt.mlab.find(m2 >= 0.25 * m2MAX)[0]
            if (IDX_0 + 50 < IDX_1) :
                interval = [x_IN[IDX_0],x_IN[IDX_1]] 
            else:
                interval = [x_IN[0],x_IN[-1]] 
            print 'Interval = ',interval
        else:
            interval = [0.0,interval] 

    if ( len(interval) == 2) :        
        IDX = plt.mlab.find(np.logical_and( x_IN >= interval[0],x_IN <= interval[1]))
        x = x_IN[IDX]
        y = y_IN[IDX]
    else:
        x = x_IN
        y = y_IN

    if np.any(np.iscomplex(y)) :
        y_re  = np.real(y)
        y_im  = np.imag(y)
        y_abs = np.abs(y)
        y_is_complex=True
    else:
        y_is_complex=False
        y_abs= y
        y_re = y
        y_im = np.zeros(y.shape)

    freq = np.linspace(0.05,4,1000)
    if ( kind == 0 ) :
        #########################################################
        ### 
        ###  Generic FIT to get a linear part plus one frequency 
        ### 
        #########################################################
        if ( (len(guess) == 0) or ( len(guess) != 5 )):
             f_max,h_max=spectrum(x,y,freq=freq,kind='int',window=3,maxima=True,norm_in='linfit',norm_out=True)
             h=spectrum(x,y,freq=freq,kind='int',window=3,showfreq=False,norm_in='linfit',norm_out=True)
             guess = [y[0],(y[-1]-y[0])/(x[-1]-x[0]),0,f_max[0],0]
        fit, pcov,info,msg,ier = scipy.optimize.leastsq(
                      trialB_RES,guess,args=(x-x[0],y)
                     ,maxfev=10000, ftol=1.e-5, xtol=1.e-8
                     ,full_output=1)
        ### print msg
        y_fit = trialB(x-x[0],fit)
    elif ( kind == 1 ) :
        #########################################################
        ### 
        ### Simple exponential growth FIT
        ### 
        #########################################################
        if ( (len(guess) == 0) or ( len(guess) != 2 ) ):
             guess = [y[0] ,  np.log(y_abs[-1]/y_abs[0])/(x[-1]-x[0]) ]
        fit, pcov,info,msg,ier = scipy.optimize.leastsq(
                       trialM_RES,guess,args=(x-x[0],y_abs)
                      ,maxfev=10000, ftol=1.e-5, xtol=1.e-8
                      ,full_output=1)
        ### print msg
        y_fit = trialM(x-x[0],fit)
    elif ( kind == 2 ) :
        #########################################################
        ### 
        ### Simple exponential growth 1-frequency  FIT  
        ### 
        #########################################################

        if ( ( len(guess) == 0 ) or ( len(guess) != 4 )):
            ########################################
            ### First Guess the exponential Factor
            #######################################
            if (y_is_complex == True ) :
                guess = [y_abs[0] ,  np.log(y_abs[-1]/y_abs[0])/(x[-1]-x[0]) ]
                fit, pcov,info,msg,ier = scipy.optimize.leastsq(trialM_RES,guess,args=(x-x[0],y_abs)
                         ,full_output=1
                         ,maxfev=10000, ftol=1.e-5, xtol=1.e-8)
                ### print msg
                y_fit = fit #trialM(x-x[0],fit)
            else :
                guess = [y[0] ,  np.log(np.max(np.abs(y))/np.max(np.abs(y[:40])))/(x[-1]-x[0]) ]
                y_fit=guess
            ########################################
            ### Now guess the frequnecy
            #######################################
            f_max,h_max=spectrum(x,y,freq=freq,kind='int',window=3,maxima=True,norm_in='mean',norm_out=True)
            h=spectrum(x,y,freq=freq,kind='int',window=3,showfreq=False,norm_in='mean',norm_out=True)
            if (len(f_max) > 0 ) :
                 guess = [y_fit[0],y_fit[1],f_max[0],np.angle(1j*y[0]) ]
                 #print "y[0]", y[0],y_fit
                 #print "Using guess", guess
            else:
                 guess = [y_fit[0],y_fit[1],1,0,0.0 ]
        try:
            fit, pcov,info,msg,ier = scipy.optimize.leastsq(trial_RES,guess,args=(x-x[0],y_re)
                         ,maxfev=10000, ftol=1.e-5, xtol=1.e-8
                         ,full_output=1,Dfun=trial_JAC,col_deriv=True)
        except:
            ier = 5
            msg = "Non-Linear fit error ! returning guess!"
            print msg
            fit = guess 
        ### print msg
        if (y_is_complex == True ) :
            y_fit = trial(x-x[0],fit) - 1j*trial_COS(x-x[0],fit)
        else :
            y_fit = trial(x-x[0],fit)
    else :
        #########################################################
        ### 
        ### Other possibilities (Not yet implemented
        ### 
        #########################################################
        print "Not yet implemented"

    if   ( (full_output == False) and (out_fit==False)) :
        return fit
    elif ( (full_output == False) and (out_fit==True)) :
        return (fit, { 'x' : x
                     , 'y' : y
                  ,'y_fit' : y_fit
                })
    else :
        if ((ier >= 1) and (ier<=4) ):
             return (fit, { 'x' : x
                          , 'y' : y
                      ,'y_fit' : y_fit
                      , 'fit'  : fit
                      , 'err'  : np.array([np.sqrt(pcov[i,i]) for i in range(len(fit))])
                      ,'pcov'  : pcov
                      ,'info'  : info
                      ,'msg'   : msg
                      ,'ier'   : ier
                   })
        else:
             return (fit, { 'x' : x
                          , 'y' : y
                      ,'y_fit' : y_fit
                      , 'fit'  : fit
                      ,'info'  : info
                      ,'msg'   : msg
                      ,'ier'   : ier
                   })
