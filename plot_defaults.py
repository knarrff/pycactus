#!/usr/bin/env python
# -*- coding: utf-8 -*-   

## import numpy as np
## import matplotlib.pyplot as plt
## import matplotlib.ticker as mticker
## 
## from matplotlib import rc
## 
## # ------------------------------
## # stuff (Plotting defaults)
## # ------------------------------
## 
## fontsize  = 10
## linewidth = 1
## rc('text', usetex=True)
## rc('font', family='serif')
## rc('font', serif='palatino')
## rc('font', weight='bolder')
## rc('mathtext', default='sf')
## rc("lines", markeredgewidth=1)
## rc("lines", linewidth=linewidth)
## rc('axes', labelsize=fontsize)
## rc("axes", linewidth=(linewidth+1)//2)
## rc('xtick', labelsize=fontsize)
## rc('ytick', labelsize=fontsize)
## rc('legend', fontsize=fontsize)
## rc('xtick.major', pad=8)
## rc('ytick.major', pad=8)
## 
## def set_tick_sizes(ax, major, minor):
##     for l in ax.get_xticklines() + ax.get_yticklines():
##         l.set_markersize(major)
##     for tick in ax.xaxis.get_minor_ticks() + ax.yaxis.get_minor_ticks():
##         tick.tick1line.set_markersize(minor)
##         tick.tick2line.set_markersize(minor)
##     ax.xaxis.LABELPAD=10.
##     ax.xaxis.OFFSETTEXTPAD=10.
## 
## palette = ['k', 'b', 'r', 'g', 'c', 'm']
## 
# constants, in SI
G     = 6.673e-11   # m^3/(kg s^2)
c     = 299792458   # m/s
M_sun = 1.98892e30  # kg
mu0   = 1.2566370614e-6 # Newton/Ampere^2
# convertion factors
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3
CU_to_Tesla = c**4 / M_sun / G**(1.5)* mu0**(0.5)
# ------------------------------------------------------------
# All expeession in CU refer to unit mass (a-dimensional) 
# where G=c=M_sun=1
# ------------------------------------------------------------
