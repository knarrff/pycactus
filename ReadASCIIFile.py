#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import getopt
 
import os
import re
import time
import numpy as np
import bz2
import gzip
import h5py as h5py
import matplotlib.pyplot as plt

### def pippo(x,search): return [item for item in list(x) if re.search(search, item)]

#################################################################################
###
###
###
###
###
###
#################################################################################

def SaveHDF5(file,data,group='',mode='a') :
    f = h5py.File(file,mode)
    if len(group) > 0 :
       try:
           grp = f.create_group(group)
       except : 
           grp = f[group]
    else :
       grp=f
    for varname in sorted(list(data)) :
       try :
           grp.create_dataset(varname, data=data[varname]) 
       except :
           if (data[varname].shape == grp[varname].shape) :
                print 'In GROUP:',group,'updating variable',varname 
                grp[varname][...]=data[varname]
           else :
                print 'In GROUP:',group,'the',varname,'as different shape',data[varname].shape,'- not -', grp[varname].shape
    f.close()

#################################################################################
###
###
###
###
###
###
#################################################################################

def ReadHDF5(file,group='') :
    f = h5py.File(file,'r')
    if len (group) > 0 :
       grp = f[group]
    else :
       grp =f
    data = dict()
    for varname in sorted(list(grp)) :
        if type(grp[varname]) == h5py._hl.dataset.Dataset :
            data[varname] = np.array(grp[varname])
    f.close()
    return data

#################################################################################
###
###
###
###
###
###
#################################################################################

#Restituisce dizionario con i dati 2d nel piano "PLANE", nel refinement level "rl"  al tempo "it". No post processing.

def Import_HDF5_2d(PATH,
     PLANE='xy',
     it=0, #initial_time
     rl=0, #refinement_level (0 -> coarser)
     VARs=['rho','eps','press','vel[0]','vel[1]','vel[2]','alp','betax','betay','betaz',
           'gxx','gxy','gxz','gyy','gyz','gzz',
          'Bvec[0]','Bvec[1]','Bvec[2]','divB']) :


     d=dict()
     for var in VARs :
         filename="%s/%s.%s.h5" %(PATH,var,PLANE)
         H5d2=h5py.File(filename)
         for pippo in list(H5d2) : 
             x = re.search('(\S*)::(\S*) it=('+str(it)+') tl=(\d*) rl=('+str(rl)+')\S*',pippo)
             if x != None :
                 print "File:",filename,"Match for", pippo 
                 delta =H5d2[pippo].attrs['delta']
                 origin=H5d2[pippo].attrs['origin']
                 sizeA =H5d2[pippo].shape
                 #if var == "rho":
                    # print "delta= ",delta,"\n origin= ", origin, "\n sizeA[0]= ",sizeA[0], "\n sizeA[1]= ", sizeA[1],"\n"
                 d[var]=np.array(H5d2[pippo])
                 tmpX=np.arange(0,sizeA[1])*delta[0]+origin[0]
                 tmpY=np.arange(0,sizeA[0])*delta[1]+origin[1]
                 # sizeA è al contrario! sizeA[0] contiene l'escursione della seconda variabile e sizeA[1] della prima
                 d[PLANE[0]],d[PLANE[1]]=np.meshgrid(tmpX, tmpY)
         H5d2.close()
    
     return d







def Get2dData(PATH,PLANE,polyK=30000,polyGamma=2.75) :
    VARs=['rho','eps','press','vel[0]','vel[1]','vel[2]']
    VARs = VARs + [ 'alp','betax','betay','betaz'] 
    VARs = VARs + [ 'gxx','gxy','gxz','gyy','gyz','gzz' ] 
    d=dict()
    for var in VARs :
        filename="%s/%s.%s.h5" %(PATH,var,PLANE)
        H5d2=h5py.File(filename)
        for pippo in list(H5d2) : 
            x = re.search('(\S*)::(\S*) it=(\d*) tl=(\d*) rl=(\d*)\S*',pippo)
            if x != None :
                print "File:",filename,"Match for", pippo 
                delta =H5d2[pippo].attrs['delta']
                origin=H5d2[pippo].attrs['origin']
                sizeA =H5d2[pippo].shape
                d[var]=np.array(H5d2[pippo])
                tmpX=np.arange(0,sizeA[0])*delta[0]+origin[0]
                tmpY=np.arange(0,sizeA[1])*delta[1]+origin[1]
                d[PLANE[0]],d[PLANE[1]]=np.meshgrid(tmpX, tmpY)
        H5d2.close()

    if re.search('x',PLANE) == None : d['x']=np.zeros(d['rho'].shape)
    if re.search('y',PLANE) == None : d['y']=np.zeros(d['rho'].shape)
    if re.search('z',PLANE) == None : d['z']=np.zeros(d['rho'].shape)
    #if re.search('press',PLANE) == None : 
    #    d['press']=polyK* d['rho']**polyGamma
    # ---- Now doing somo post processing
    d['sqrtg'] = np.sqrt(d['gxx']*d['gyy']*d['gzz']+2.0*d['gxy']*d['gxz']*d['gyz']
                  - d['gxy'] * d['gxy'] * d['gzz']- d['gxz'] * d['gxz'] * d['gyy']
                  - d['gyz'] * d['gyz'] * d['gxx']) 
    d['V2'] = ( d['gxx'] * d['vel[0]'] * d['vel[0]'] 
      + d['gyy'] * d['vel[1]'] * d['vel[1]'] 
      + d['gzz'] * d['vel[2]'] * d['vel[2]']
      + 2.0 * d['gxy'] * d['vel[0]'] * d['vel[1]'] 
      + 2.0 * d['gxz'] * d['vel[0]'] * d['vel[2]'] 
      + 2.0 * d['gyz'] * d['vel[1]'] * d['vel[2]'] 
       )
    d['W'] = 1 / np.sqrt(1 - d['V2'])
    d['u0'] = d['W'] / d['alp']
    d['ux'] = d['W']  * (d['vel[0]'] - d['betax'] / d['alp']);
    d['uy'] = d['W']  * (d['vel[1]'] - d['betay'] / d['alp']);
    d['uz'] = d['W']  * (d['vel[2]'] - d['betaz'] / d['alp']);
    ###### ----------******************************************
    d['uD0'] =(-d['alp'] + d['gxx'] * d['vel[0]'] * d['betax'] 
               + d['gxy'] * d['vel[0]'] * d['betay'] 
               + d['gxz'] * d['vel[0]'] * d['betaz'] 
               + d['gxy'] * d['vel[1]'] * d['betax'] 
               + d['gyy'] * d['vel[1]'] * d['betay'] 
               + d['gyz'] * d['vel[1]'] * d['betaz'] 
               + d['gxz'] * d['vel[2]'] * d['betax'] 
               + d['gyz'] * d['vel[2]'] * d['betay'] 
               + d['gzz'] * d['vel[2]'] * d['betaz'] 
            ) * d['W']
    ###### ----------******************************************
    d['uDx'] = d['W'] * ( + d['gxx'] * d['vel[0]'] 
                     + d['gxy'] * d['vel[1]'] 
                     + d['gxz'] * d['vel[2]'])
    d['uDy'] = d['W'] * ( + d['gxy'] * d['vel[0]'] 
                     + d['gyy'] * d['vel[1]'] 
                     + d['gyz'] * d['vel[2]'])
    d['uDz'] = d['W'] * ( + d['gxz'] * d['vel[0]'] 
                     + d['gyz'] * d['vel[1]']
                     + d['gzz'] * d['vel[2]'])
    ###### ----------******************************************
    d['betaDx'] =  ( + d['gxx'] * d['betax'] 
                     + d['gxy'] * d['betay'] 
                     + d['gxz'] * d['betaz'])
    d['betaDy'] =  ( + d['gxy'] * d['betax'] 
                     + d['gyy'] * d['betay'] 
                     + d['gyz'] * d['betaz'])
    d['betaDz'] = (  + d['gxz'] * d['betax'] 
                     + d['gyz'] * d['betay']
                     + d['gzz'] * d['betaz'])
    ###### ----------******************************************
    d['rho*'] = d['sqrtg'] * d['W'] * d['rho']
    d['e0']   = d['rho'] *( 1.0 + d['eps'] ) + d['press']
    ###### ----------******************************************
    d['R']     = np.sqrt(d['x']**2+d['y']**2+d['z']**2)
    d['Rcyl']  = np.sqrt(d['x']**2+d['y']**2)
    d['Omega'] = (d['x'] * d['uy'] - d['y'] * d['ux'] ) / d['u0'] / (d['Rcyl']**2+1e-60)
    i,j=np.where(d['Rcyl']< 1e-12)
    for idx in range(i.size) : 
        ##  d['Omega'][i[idx],j[idx]] = d['Omega'][i[idx],j[idx]+1]
        d['Omega'][i[idx],j[idx]] = (1/6.0) * (
                      4*d['Omega'][i[idx],j[idx]+1]-d['Omega'][i[idx],j[idx]+2]
                     +4*d['Omega'][i[idx],j[idx]-1]-d['Omega'][i[idx],j[idx]-2]
                     )
    ###### ----------******************************************
    d['gtphi']    = - d['y'] * d['betaDx'] + d['x'] * d['betaDy'] 
    d['gphiphi']  = ( d['gxx'] * d['y'] * d['y']
                    + d['gyy'] * d['x'] * d['x']
                    -2* d['gxy'] * d['x'] * d['y']
                    ) 
    d['gtt'] =( - d['alp'] * d['alp']  
               + d['betaDx'] * d['betax'] 
               + d['betaDy'] * d['betay'] 
               + d['betaDz'] * d['betaz'] )
    d['omega']    = - d['gtphi'] / (d['gphiphi']+1e-50)
    # -------------------------------------------------------
    #  d['g_rot'] is the term entering in the rotation Law
    #          = r^2 sin^2(theta) exp(-2 nu)
    # -------------------------------------------------------
    d['mask']     = 1.0*(d['rho']> d['rho'].max()*1e-5)
    d['g_rot']    = - d['gphiphi']/d['gtt']
    d['NUM_rot']  = (d['Omega']-d['omega'])*d['g_rot'] 
    d['DEN_rot']  = 1-(d['Omega']-d['omega'])**2 *d['g_rot'] 
    d['LAW_rot']  = d['NUM_rot']/d['DEN_rot']
    i,j=np.where(d['Rcyl']< 1e-12)
    for idx in range(i.size) : 
        ## d['omega'][i[idx],j[idx]] = d['omega'][i[idx],j[idx]+1]
        d['omega'][i[idx],j[idx]] = (1/6.0) * (
                      4*d['omega'][i[idx],j[idx]+1]-d['omega'][i[idx],j[idx]+2]
                     +4*d['omega'][i[idx],j[idx]-1]-d['omega'][i[idx],j[idx]-2]
                    )
    ###### ----------******************************************
    # constants, in SI
    G     = 6.673e-11   # m^3/(kg s^2)
    c     = 299792458   # m/s
    M_sun = 1.98892e30  # kg
    # convertion factors
    CU_to_km   = M_sun*G/(1000*c*c)                  # km
    CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
    CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
    CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3
    print 'Max matter coordinate frequency in kHz:',np.max(d['mask']*d['Omega'])/(2.0*np.pi)/CU_to_ms
    print 'Min matter coordinate frequency in kHz:',1/np.max(d['mask']/d['Omega'])/(2.0*np.pi)/CU_to_ms
    return d

def Get2XData(PATH,PLANE,polyK=30000,polyGamma=2.75,rl='0',it='0') :
    VARs=['rho','eps','press','vel[0]','vel[1]','vel[2]']
    VARs = VARs + [ 'alp','betax','betay','betaz'] 
    VARs = VARs + [ 'gxx','gxy','gxz','gyy','gyz','gzz' ] 
    VARs = VARs + [ 'Bvec[0]','Bvec[1]','Bvec[2]' ] 
    d=dict()
    #print 'PIPPO'
    for var in VARs :
        filename="%s/%s.%s.h5" %(PATH,var,PLANE)
        H5d2=h5py.File(filename)
        for pippo in list(H5d2) : 
            x = re.search('(\S*)::(\S*) it=(\d*) tl=(\d*) rl=(\d*)\S*',pippo)
            if x != None :
                xRL=x.group(5)
                xIT=x.group(3)
                if (xRL==rl) & (xIT == it) :
                    print "File:",filename,"Match for", pippo 
                    delta =H5d2[pippo].attrs['delta']
                    origin=H5d2[pippo].attrs['origin']
                    sizeA =H5d2[pippo].shape
                    d[var]=np.array(H5d2[pippo])
                    tmpX=np.arange(0,sizeA[1])*delta[0]+origin[0]
                    tmpY=np.arange(0,sizeA[0])*delta[1]+origin[1]
                    #print 'sizeA=',sizeA
                    #print 'tmpX=',tmpX
                    #print 'tmpY=',tmpY
                    d[PLANE[0]],d[PLANE[1]]=np.meshgrid(tmpX,tmpY)
        H5d2.close()

    if re.search('x',PLANE) == None : d['x']=np.zeros(d['rho'].shape)
    if re.search('y',PLANE) == None : d['y']=np.zeros(d['rho'].shape)
    if re.search('z',PLANE) == None : d['z']=np.zeros(d['rho'].shape)
    #if re.search('press',PLANE) == None : 
    #    d['press']=polyK* d['rho']**polyGamma
    # ---- Now doing somo post processing
    d['sqrtg'] = np.sqrt(d['gxx']*d['gyy']*d['gzz']+2.0*d['gxy']*d['gxz']*d['gyz']
                  - d['gxy'] * d['gxy'] * d['gzz']- d['gxz'] * d['gxz'] * d['gyy']
                  - d['gyz'] * d['gyz'] * d['gxx']) 
    d['V2'] = ( d['gxx'] * d['vel[0]'] * d['vel[0]'] 
      + d['gyy'] * d['vel[1]'] * d['vel[1]'] 
      + d['gzz'] * d['vel[2]'] * d['vel[2]']
      + 2.0 * d['gxy'] * d['vel[0]'] * d['vel[1]'] 
      + 2.0 * d['gxz'] * d['vel[0]'] * d['vel[2]'] 
      + 2.0 * d['gyz'] * d['vel[1]'] * d['vel[2]'] 
       )
    d['W'] = 1 / np.sqrt(1 - d['V2'])
    d['u0'] = d['W'] / d['alp']
    d['ux'] = d['W']  * (d['vel[0]'] - d['betax'] / d['alp']);
    d['uy'] = d['W']  * (d['vel[1]'] - d['betay'] / d['alp']);
    d['uz'] = d['W']  * (d['vel[2]'] - d['betaz'] / d['alp']);
    ###### ----------******************************************
    d['uD0'] =(-d['alp'] + d['gxx'] * d['vel[0]'] * d['betax'] 
               + d['gxy'] * d['vel[0]'] * d['betay'] 
               + d['gxz'] * d['vel[0]'] * d['betaz'] 
               + d['gxy'] * d['vel[1]'] * d['betax'] 
               + d['gyy'] * d['vel[1]'] * d['betay'] 
               + d['gyz'] * d['vel[1]'] * d['betaz'] 
               + d['gxz'] * d['vel[2]'] * d['betax'] 
               + d['gyz'] * d['vel[2]'] * d['betay'] 
               + d['gzz'] * d['vel[2]'] * d['betaz'] 
            ) * d['W']
    ###### ----------******************************************
    d['uDx'] = d['W'] * ( + d['gxx'] * d['vel[0]'] 
                     + d['gxy'] * d['vel[1]'] 
                     + d['gxz'] * d['vel[2]'])
    d['uDy'] = d['W'] * ( + d['gxy'] * d['vel[0]'] 
                     + d['gyy'] * d['vel[1]'] 
                     + d['gyz'] * d['vel[2]'])
    d['uDz'] = d['W'] * ( + d['gxz'] * d['vel[0]'] 
                     + d['gyz'] * d['vel[1]']
                     + d['gzz'] * d['vel[2]'])
    ###### ----------******************************************
    d['betaDx'] =  ( + d['gxx'] * d['betax'] 
                     + d['gxy'] * d['betay'] 
                     + d['gxz'] * d['betaz'])
    d['betaDy'] =  ( + d['gxy'] * d['betax'] 
                     + d['gyy'] * d['betay'] 
                     + d['gyz'] * d['betaz'])
    d['betaDz'] = (  + d['gxz'] * d['betax'] 
                     + d['gyz'] * d['betay']
                     + d['gzz'] * d['betaz'])
    ###### ----------******************************************
    d['rho*'] = d['sqrtg'] * d['W'] * d['rho']
    d['e0']   = d['rho'] *( 1.0 + d['eps'] ) + d['press']
    ###### ----------******************************************
    d['R']     = np.sqrt(d['x']**2+d['y']**2+d['z']**2)
    d['Rcyl']  = np.sqrt(d['x']**2+d['y']**2)
    d['Omega'] = (d['x'] * d['uy'] - d['y'] * d['ux'] ) / d['u0'] / (d['Rcyl']**2+1e-60)
    i,j=np.where(d['Rcyl']< 1e-12)
    if PLANE == 'xy':
        for idx in range(i.size) : 
           ##  d['Omega'][i[idx],j[idx]] = d['Omega'][i[idx],j[idx]+1]
           d['Omega'][i[idx],j[idx]] = (1/6.0) * (
                         4*d['Omega'][i[idx],j[idx]+1]-d['Omega'][i[idx],j[idx]+2]
                        +4*d['Omega'][i[idx],j[idx]-1]-d['Omega'][i[idx],j[idx]-2]
                        )
    ###### ----------******************************************
    d['gtphi']    = - d['y'] * d['betaDx'] + d['x'] * d['betaDy'] 
    d['gphiphi']  = ( d['gxx'] * d['y'] * d['y']
                    + d['gyy'] * d['x'] * d['x']
                    -2* d['gxy'] * d['x'] * d['y']
                    ) 
    d['gtt'] =( - d['alp'] * d['alp']  
               + d['betaDx'] * d['betax'] 
               + d['betaDy'] * d['betay'] 
               + d['betaDz'] * d['betaz'] )
    d['omega']    = - d['gtphi'] / (d['gphiphi']+1e-50)
    # -------------------------------------------------------
    #  d['g_rot'] is the term entering in the rotation Law
    #          = r^2 sin^2(theta) exp(-2 nu)
    # -------------------------------------------------------
    d['mask']     = 1.0*(d['rho']> d['rho'].max()*1e-5)
    d['g_rot']    = - d['gphiphi']/d['gtt']
    d['NUM_rot']  = (d['Omega']-d['omega'])*d['g_rot'] 
    d['DEN_rot']  = 1-(d['Omega']-d['omega'])**2 *d['g_rot'] 
    d['LAW_rot']  = d['NUM_rot']/d['DEN_rot']
    i,j=np.where(d['Rcyl']< 1e-12)
    if PLANE == 'xy':
        for idx in range(i.size) : 
            ## d['omega'][i[idx],j[idx]] = d['omega'][i[idx],j[idx]+1]
            d['omega'][i[idx],j[idx]] = (1/6.0) * (
                          4*d['omega'][i[idx],j[idx]+1]-d['omega'][i[idx],j[idx]+2]
                         +4*d['omega'][i[idx],j[idx]-1]-d['omega'][i[idx],j[idx]-2]
                        )
    ###### ----------******************************************
    # constants, in SI
    G     = 6.673e-11   # m^3/(kg s^2)
    c     = 299792458   # m/s
    M_sun = 1.98892e30  # kg
    # convertion factors
    CU_to_km   = M_sun*G/(1000*c*c)                  # km
    CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms
    CU_to_dens = c*c*c*c*c*c / (G*G*G * M_sun*M_sun) # kg/m^3
    CU_to_dens_CGS = CU_to_dens *1000/100**3         # g/cm^3
    #print 'Max matter coordinate frequency in kHz:',np.max(d['mask']*d['Omega'])/(2.0*np.pi)/CU_to_ms
    #print 'Min matter coordinate frequency in kHz:',1/np.max(d['mask']/d['Omega'])/(2.0*np.pi)/CU_to_ms
    return d

#################################################################################
###
###
###
###
###
###
#################################################################################

def ReadHDF5datasets(file,RE,group='',data=False,attr=False) :
    f = h5py.File(file,'r')
    if len (group) > 0 :
       grp = f[group]
    else :
       grp =f
    datas = []
    attrs = dict()
    for dsetname in sorted(list(grp)) :
        ## print dsetname
        if re.search(RE,dsetname) :
            infos=dict()
            infos['dsetname'] = dsetname
            if attr :
                infos['attr'] = dict([(item,grp[dsetname].attrs[item]) for item in list(grp[dsetname].attrs)])
            ## print 'INFOS=',infos
            if type(grp[dsetname]) == h5py._hl.dataset.Dataset :
                ## print "Is a dataset! See if a load has been requested"
                if data :
                    infos['data'] = np.array(grp[dsetname])
            #  print "Matched var name",dsetname
            ########################################################
            #  Getting info about the component IF is a CARPET file
            ########################################################
            REG = re.match('(\S+)::(\S+) it=(\d+)',dsetname)
            if REG :
                infos['type']  = 'carpet'
                infos['carpet']=dict()
                infos['carpet']['group'] = REG.groups()[0]
                infos['carpet']['var']   = REG.groups()[1]
                infos['carpet']['it']    = int(REG.groups()[2])
                REG = re.search('tl=(\d+)',dsetname); 
                if REG: infos['carpet']['tl']=int(REG.groups()[0])
                REG = re.search('rl=(\d+)',dsetname)
                if REG: infos['carpet']['rl=']=int(REG.groups()[0])
                REG = re.search('c=(\d+)',dsetname)
                if REG: infos['carpet']['c=']=int(REG.groups()[0])
                #######################################################
                ## if is a carpet DATASET and there are the appropriate
                ## attribute create the underlining coordinate grid
                #######################################################
                if attr and data :
                    #################################################
                    ### Note a data set that appear to h5ls as 34x65
                    ### correspond to:
                    ###   0= orig[0]+delta[0] (0:64) 
                    ###   1= orig[1]+delta[1] (0:33) 
                    ###
                    ###  data ARE shaped as 34x64
                    #################################################
                    delta =infos['attr']['delta']
                    origin=infos['attr']['origin']
                    size  =infos['data'].shape
                    dim = len(size)
                    infos['carpet']['size'] = size
                    infos['carpet']['dim'] = dim
                    for i in range(dim) :
                         infos['carpet'][i] = np.arange(0,size[(dim-1)-i])*delta[i]+origin[i]
            else:
                infos['type']  = 'unknown'
            datas.append(infos)
    f.close()
    return datas

#################################################################################
###
###
###
###
###
###
#################################################################################

def ReadASCIIFileRE(search_dir,matchs='.asc',verbose=False,simfactory=True) :
     if type(matchs) == str :
         matchs=[matchs]
     matchedFILEs = []
     
     ### ######################################################
     ### If search_dir is not a directory checj if it is
     ### FILENAME (in this case read the content of that file) 
     ##########################################################
     if not os.path.isdir(search_dir) :
         print search_dir,"is not a directory"
         if os.path.exists(search_dir) :
             return ReadASCIIFile(search_dir,verbose=verbose,simfactory=False)
         else :
             return dict()
     ### ###################################################
     ### If simfactory is set to yes check if dir is instead
     ### the base dir not of the files but of a sifactory 
     ### simulation 
     ### ###################################################
     search_dirs = []
     #######################################################
     ### Check if the directory has a symfactory layout 
     #######################################################
     simfactory = False
     #### ---------------------------------------------
     ####  Search for tree of the kind:
     ####
     ####  search_dir/output-????/[parname]/data/Scalar  
     ####                                       /0d
     ####                                       /1d
     #### ---------------------------------------------
     for dir_entry in sorted(os.listdir(search_dir)) :
          if re.match(r"^output-(\d\d\d\d)$",dir_entry) :
              if (os.path.exists(os.path.join(search_dir, "SIMFACTORY", "par"))):
                  # Find parfile name and assume out_dir was set to $parfile
                  parfiles = os.listdir(os.path.join(search_dir, "SIMFACTORY", "par"))
                  if (".svn" in parfiles): parfiles.remove(".svn")
                  if (len(parfiles) == 1) :
                      parfile = parfiles[0]
                      parfile = re.sub(r".par$", "", parfile)
                      if os.path.exists(os.path.join(search_dir, dir_entry, parfile)) :
                          search_dirs.append(os.path.join(search_dir, dir_entry,parfile))
                          for subdir in ['Scalar', '0d', '1d']:
                              if (os.path.exists(os.path.join(search_dir, dir_entry,parfile, 'data', subdir))) :
                                  search_dirs.append(os.path.join(search_dir, dir_entry,parfile, 'data', subdir))
                  simfactory = True
              else :
                  # There is no SIMFACTORY DIR look for "data" dir]
                  for subdir in ['Scalar', '0d', '1d']:
                      if (os.path.exists(os.path.join(search_dir, dir_entry, 'data', subdir))) :
                          search_dirs.append(os.path.join(search_dir, dir_entry, 'data', subdir))
                  if (len(search_dirs) == 0):
                      if os.path.isdir(os.path.join(search_dir, dir_entry)) == True :
                          for subdir_entry in sorted(os.listdir(os.path.join(search_dir, dir_entry))) :
                              for subdir in ['Scalar', '0d', '1d']:
                                  if (os.path.exists(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))) :
                                      search_dirs.append(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))   
                  simfactory = True          
              break          
     # ----------------############ ------------------
     # IF not-simfactory look for data dir    
     # ----------------############ ------------------
     if simfactory == False :
         #### ---------------------------------------------
         ####  Search for tree of the kind:
         ####
         ####  search_dir/[parname]/data/Scalar  
         ####                           /0d
         ####                           /1d
         #### ---------------------------------------------
         for dir_entry in sorted(os.listdir(search_dir)) :
             # There is no SIMFACTORY DIR look for "data" dir]
             for subdir in ['Scalar', '0d', '1d']:
                 if (os.path.exists(os.path.join(search_dir, dir_entry, 'data', subdir))) :
                     search_dirs.append(os.path.join(search_dir, dir_entry, 'data', subdir))
             if (len(search_dirs) == 0):
                 if os.path.isdir(os.path.join(search_dir, dir_entry)) == True :
                     for subdir_entry in sorted(os.listdir(os.path.join(search_dir, dir_entry))) :
                         for subdir in ['Scalar', '0d', '1d']:
                             if (os.path.exists(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))) :
                                 search_dirs.append(os.path.join(search_dir, dir_entry, subdir_entry, 'data', subdir))   
     # ----------------############ ------------------
     # Treat non-simfactory layout as flat directory    
     # ----------------############ ------------------
     if (len(search_dirs) == 0):
          search_dirs.append(search_dir)      
     for basedir in search_dirs :  
         for file_entry in sorted(os.listdir(basedir)) :
             for match in matchs :
                 if re.search(match, file_entry) :
                     matchedFILE=os.path.join(basedir,file_entry)
                     if matchedFILE not in matchedFILEs :
                         matchedFILEs.append(matchedFILE) 

     out = dict()
     for file in matchedFILEs :
         out.update(ReadASCIIFile(file,verbose=verbose,simfactory=simfactory))
     return out

#################################################################################
###
###
###
###
###
###
#################################################################################
def GetHeaderReadASCIIFile(infile) :
     sname, ext   = os.path.splitext(infile)
     ##print infile[0],extension[0]
     if (ext == '.bz2') :
         f = bz2.BZ2File(infile)   
     elif ( ext == '.gz' ) :
         f = gzip.GzipFile(infile)
     else :
         f = open(infile)
     # 
     header=[]
     for line in f.readlines():
       if "# 1:iteration 2:time 3:data" in line:
         header = header + line.split()[1:]
       if "# column format:" in line:
         header = line.split()[3:]
       if "# data columns: " in line:
         del header[-1]
         header = header + line.split()[3:]
         break
     f.close()
     names=[]
     if ( len(header) > 0 )  :
         colnum = range(len(header)) 
         for c, name in enumerate(header):
             assert(int(name.split(":")[0]) == c+1 )
             names = names + [name.split(":")[1] ]  
     return names
     
def ReadASCIIFile(full_file_name,verbose=False,simfactory=True) :
     out = dict()
     if not os.path.exists(full_file_name)  :
        print "File, full_file_name, do not exists:"
        return
     path , fname = os.path.split(full_file_name)
     sname, ext   = os.path.splitext(fname)
     if  ( (ext == '.bz2') or (ext == '.gz' ) ) :
         vname, ext2 = os.path.splitext(sname)
     else :
         vname = sname

     if verbose : print "Reading from file: ",full_file_name
     ## print "var Name    :",vname 
     ## print "Path: ",path
     ## print "Full Name   :",fname 
     ## print "Search Name :",sname 
     ## print "Extension :",ext 
     # --------------------------------------------------------
     # --- Now we look if full_file_name as a SIMFACTORY layout
     # --------------------------------------------------------
     SIMFACTORY=dict()
     nr = re.search("(\S*)/output-(\d\d\d\d)/(\S)",full_file_name)
     if (nr) :
         SIMFACTORY["layout"]     = True
         SIMFACTORY["basedir"]    = nr.group(1)
         SIMFACTORY["iteration"]  = int(nr.group(2))
         SIMFACTORY["files"]  = list()
         SIMFACTORY["ext"]    = list()
         for dir_entry in sorted(os.listdir(SIMFACTORY["basedir"])) :
              ## print "============================="
              ## print "Scanning",dir_entry
              nrDIR = re.match(r"^output-(\d\d\d\d)$", dir_entry)
              if ( nrDIR ) :
                   ## print "GOT match =", dir_entry
                   newPATH = re.sub("output-\d\d\d\d",dir_entry,path)
                   if os.path.exists(newPATH) :
                       ## print "Find path xx =", newPATH
                       for file_entry in os.listdir(newPATH) :
                            ## print file_entry,"...",sname
                            nrFILE = re.search(sname+"\S*", file_entry)
                            if (nrFILE) :
                                ## print "Got file MATCH:", file_entry
                                SIMFACTORY["files"].append(os.path.join(newPATH, file_entry))
                                SIMFACTORY["ext"].append(os.path.splitext(file_entry)[1])
         ## print "SIMFACTORY Simulation dir:", SIMFACTORY["basedir"] 
         ## print "SIMFACTORY Run Numbers:", SIMFACTORY["iteration"]
         infile    = SIMFACTORY["files"];
         extension = SIMFACTORY["ext"];
     else :
         SIMFACTORY["layout"] = False
         infile    = [full_file_name];
         extension = [ext];
     # -----------------------------------------------------
     # --- Now we cat start reading the content of the FILES
     # -----------------------------------------------------
     ##print infile[0],extension[0]
     if (extension[0] == '.bz2') :
         f = bz2.BZ2File(infile[0])   
     elif ( extension[0] == '.gz' ) :
         f = gzip.GzipFile(infile[0])
     else :
         f = open(infile[0])
     # 
     header=[]
     for line in f.readlines():
       if "# 1:iteration 2:time 3:data" in line:
         header = header + line.split()[1:]
       if "# column format:" in line:
         header = line.split()[3:]
       if "# data columns: " in line:
         del header[-1]
         header = header + line.split()[3:]
         break
     f.close()
     # parse individial name fields  
     names=[]
     if ( len(header) > 0 )  :
         colnum = range(len(header)) 
         for c, name in enumerate(header):
             assert(int(name.split(":")[0]) == c+1 )
             names = names + [name.split(":")[1] ]  
             ## print name,names[c], "-->",c,colnum[c]
         # now replace entries in incols with numbers
         ## print header
     ## else :
     ## print "NOHEADER",names 

     ## -------- Get the data from the first file --------
     try:
         data = np.loadtxt(infile[0], comments="#", unpack=True)
     except :
         if names[0]== 'name' :
             data = np.loadtxt(infile[0], comments="#", usecols=colnum[1:],unpack=True)
             out['name']=np.genfromtxt(infile[0], comments="#", usecols=(0),unpack=True,dtype=np.str)
             colnum = [ii-1 for ii in colnum[1:]]
             names  = names[1:]
             ## print names
             ## print colnum
             u, indices = np.unique(data[0,:], return_index=True)
             for i , name in enumerate(names) :
                  out[name] = data[colnum[i],indices]
 
             return out
         else:
             print "[ERROR 0] Unable to load file:", infile[0]
             try: 
                 # data = np.genfromtxt(infile[0], comments="#", unpack=True)
                 wrongFILE = open(infile[0])
                 goodFILE  = open('/tmp/ReadASCIIFile.py.txt','w')
                 for wrongTMP in (wrongFILE.readlines()) :
                     if wrongTMP[0].isalnum():
                         goodFILE.writelines(wrongTMP)
                 wrongFILE.close()
                 goodFILE.close()
                 data = np.loadtxt('/tmp/ReadASCIIFile.py.txt', comments="#",unpack=True)
                 os.remove('/tmp/ReadASCIIFile.py.txt')
             except :
                 print "[ERROR 1] Unable to load file:", infile[0]
                 return out

     # -----------------------------------------------------
     # If header are not present I should try to find out
     # the meaning of the data
     # -----------------------------------------------------
     if ( len(names) == 0 )  :
         Ncolumn,Nrow=np.shape(data)
         ## print Ncolumn,Nrow
         if ( Ncolumn == 3 ) :
             ## print "This is a scalar var"
             names  = ['iteration', 'time',vname]
             colnum = [0,1,2]
         elif ( Ncolumn == 13 ) :
             names  = ['iteration', 'time',vname]
             colnum = [0,8,12]

     # -----------------------------------------------------
     # Now read data form the other components
     # -----------------------------------------------------
     for i in range(1,len(infile)) :
         ## print i
         try :
             tmp = np.loadtxt(infile[i], comments="#", unpack=True)
             data = np.append(data,tmp,axis=1)
         except :
             print "[ERROR] Unable to load file:", infile[i]

     # if there is only one row of data the array is 1d instead of 2d
     # Correct it!   
     if np.size(data.shape) == 1 :
         data = data.reshape(np.size(data),1)
     u, indices = np.unique(data[0,:], return_index=True)
     for i , name in enumerate(names) :
         out[name] = data[colnum[i],indices]
 
     return out

#################################################################################
###
###
###    M A I N 
###
###
#################################################################################


## -------------------------------------------------------------
##  This functions can be easly used as line tools
## -------------------------------------------------------------

if __name__ == "__main__":

    usage = "usage: "+ sys.argv[0]+" [-hv] [-p <plotvar>] [-s <shovar>] [-b BaseDir ] REGvars <dirs>"

    #################################
    # PARSE command line arguments
    #################################
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hvp:s:b:i:o:",["ifile=","ofile="])
    except :
        print(usage)
        sys.exit(2)
    #################################
    # END PARSE command line arguments
    #################################
    if (len(args) < 2):
        print(usage)
        sys.exit(2)

    Regvars=args[0]
    sims = args[1:]
    idx = 0;
    BaseDir = '';
    for opt, arg in opts:
        if ( opt == '-v') :
            print "OPTIONS: ",opts 
            print "REGVARS: ",Regvars 
            for i,sim in enumerate(sorted(sims)):
                print "SIM(",i,")=",sim
        elif (opt == '-b') :
             BaseDir = arg

    for sim in sorted(sims):
        try :
            x = ReadASCIIFileRE(os.path.join(BaseDir,sim),Regvars,verbose=False)
            ####################################################### 
            ## check for optional actions and fo what it is request 
            ####################################################### 
            for opt, arg in opts:
                if opt == '-s' :
                    if arg =='all' :
                        vars=x.keys()
                    else :
                        vars= [arg]
                    for var in vars :
                        print "[",sim,"]",var,"in (", min(x[var]),max(x[var]),")"
                if opt == '-p' :
                    idx = idx + 1
                    plotname='plot_'+str(idx)+"_"+arg+".pdf"
                    print "Creating plot ", plotname
                    fig = plt.figure(figsize=(8, 4))
                    fig.subplots_adjust(top=0.95, bottom=0.15, left=0.1,right=0.98, wspace=0.25)
                    ax = fig.add_subplot(1,1,1)
                    ax.plot(x['time'],x[arg])
                    ax.set_title(sim)
                    plt.savefig(plotname, transparent=True)
                    fig.clear()

            ####################################################### 
            ####################################################### 
        except :
            print "Unable to process:",sim,'vars=',Regvars
