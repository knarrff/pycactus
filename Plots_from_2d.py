#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import getopt
 
import os
import re
import time
import numpy as np
import bz2
import gzip
import h5py as h5py
import matplotlib.pyplot as plt
from bisect import bisect_left
sys.path.append('./')
from ReadASCIIFile import *


########################################################################
# Utilities to plot 2d pseudocolor or 1d representations of a variable #
# at a given time from Cactus HDF5 output,                             #
# even for datasets with multiple components.                          #
########################################################################


G     = 6.673e-11   # m^3/(kg s^2)
c     = 299792458   # m/s
M_sun = 1.98892e30  # kg
# convertion factors
CU_to_km   = M_sun*G/(1000*c*c)                  # km
CU_to_ms   = (1000*M_sun*G/(c*c*c))              # ms

##############################################################################
# time2iteration                                                             #
# returns the iteration number multiple of HDF5_output_frequency in which    #
# the simulations has reached a time closer to the input one.                #
# "H52d_PATH" is the path to the H5_2d folder in a simfactory-like structure.#
# "time" is the input time in ms.                                            #
# "HDF5_output_frequency" is the value of "CarpetIOHDF5::out2D_every" set    #
# in the parameter file of the simulation (could be set automatically in a   #
# future update)                                                             #
##############################################################################

def time2iteration(H52d_PATH,time,HDF5_output_frequency=16384):
    
    print 'H5_2d path = ', H52d_PATH
    data = ReadASCIIFile(H52d_PATH+'/../Scalar/hydrobase::rho.maximum.asc')
    it=[]
    t=[]
    for i in range(len(data['iteration'])):
        if (data['iteration'][i] % HDF5_output_frequency == 0):
            it.append(data['iteration'][i])
            t.append(data['time'][i]*CU_to_ms)
    bl = bisect_left(t,time)
    if t[bl] - time > 0.5:
        bl = bl-1
    return int(it[bl]) 


##############################################################################
# Import_Hdf5_2d                                                             #
# returns an array of dictionaries (one element for every component) with    #
# the keys "var", "PLANE[0]" and "PLANE[1]".                                 #
# "PATH" is the path to the simulation base folder                           #
#  in a simfactory-like structure.                                           #
# "PLANE" in "xy" or "yz" or "xz".                                           #
# "time" is the input time in ms.                                            #
# "rl" is the refinement level (0 -> coarser)                                #
# "var" is the variable to be read from the hdf5 datasets                    #    
# "HDF5_output_frequency" is the value of "CarpetIOHDF5::out2D_every" set    #
# in the parameter file of the simulation (could be set automatically in a   #
# future update)                                                             #
##############################################################################

def Import_HDF5_2d(PATH,
                   PLANE='xy',
                   time=0,
                   rl=0, #refinement_level (0 -> coarser)
                   var='rho',
                   HDF5_output_frequency=16384) :
    

    d=dict()
    d[var]=[]
    d[PLANE[0]]=[]
    d[PLANE[1]]=[]
    MatchedFILEs=[]
    
    
    if not os.path.isdir(PATH) :
         print PATH,"is not a directory"
         if os.path.exists(PATH) :
             return dict() #TODO: cerca ../Scalar
         else :
             return dict()
    

    #Looking for the H5_2d folders in some possible simfactory-like structures.
    #Taken from ReadASCIIFileRe and modified where necessary.
    simfactory = False
    search_dirs = []
    filename="%s.%s.h5" %(var,PLANE)
    for dir_entry in sorted(os.listdir(PATH)) :
        if re.match(r"^output-(\d\d\d\d)$",dir_entry) :
            if (os.path.exists(os.path.join(PATH, "SIMFACTORY", "par"))):
                parfiles = os.listdir(os.path.join(PATH, "SIMFACTORY", "par"))
                if (".svn" in parfiles): parfiles.remove(".svn")
                if (len(parfiles) == 1) :
                    parfile = parfiles[0]
                    parfile = re.sub(r".par$", "", parfile)
                    print 'parfile =', parfile
                    if os.path.exists(os.path.join(PATH, dir_entry, parfile)) :
                        for subdir in ['H5_2d', '2d']:
                            if (os.path.exists(os.path.join(PATH, dir_entry,parfile, 'data', subdir))) :
                                search_dirs.append(os.path.join(PATH, dir_entry,parfile, 'data', subdir))
                simfactory = True
            else :
                for subdir in ['H5_2d','2d']:
                    if (os.path.exists(os.path.join(PATH, dir_entry, 'data', subdir))) :
                        search_dirs.append(os.path.join(PATH, dir_entry, 'data', subdir))
                if (len(search_dirs) == 0):
                    if os.path.isdir(os.path.join(PATH, dir_entry)) == True :
                        for subdir_entry in sorted(os.listdir(os.path.join(PATH, dir_entry))) :
                            for subdir in ['H5_2d', '2d']:
                                if (os.path.exists(os.path.join(PATH, dir_entry, subdir_entry, 'data', subdir))) :
                                    search_dirs.append(os.path.join(PATH, dir_entry, subdir_entry, 'data', subdir))   
                simfactory = True          
    if simfactory == False :
        for dir_entry in sorted(os.listdir(PATH)) :
             for subdir in ['H5_2d', '2d']:
                 if (os.path.exists(os.path.join(PATH, dir_entry, 'data', subdir))) :
                     search_dirs.append(os.path.join(PATH, dir_entry, 'data', subdir))
             if (len(search_dirs) == 0):
                 if os.path.isdir(os.path.join(PATH, dir_entry)) == True :
                     for subdir_entry in sorted(os.listdir(os.path.join(PATH, dir_entry))) :
                         for subdir in ['H5_2d', '2d']:
                             if (os.path.exists(os.path.join(PATH, dir_entry, subdir_entry, 'data', subdir))) :
                                 search_dirs.append(os.path.join(PATH, dir_entry, subdir_entry, 'data', subdir))   
    if (len(search_dirs) == 0):
        search_dirs.append(PATH)      
#Construct array of all matching files found:
    for basedir in search_dirs :
        print basedir
        print filename
        for file_entry in sorted(os.listdir(basedir)) :
            if filename == file_entry:
                matchedFILE=os.path.join(basedir,file_entry)
                print matchedFILE
                if matchedFILE not in MatchedFILEs :
                    MatchedFILEs.append(matchedFILE) 
    
#Convert time to iteration number to read the closer corresponding dataset:
    it = time2iteration(search_dirs[0],time,HDF5_output_frequency)

#For every matching file, look for an appropriate dataset ad update the dictionary with its values
    found = False
    for file in MatchedFILEs:
        print "file=", file
        H5d2=h5py.File(file,'r')
        for pippo in list(H5d2) :
            x = re.search('(\S*)::(\S*) it=('+str(it)+') tl=(\d*) rl=('+str(rl)+')\S*',pippo)
            if x != None :
                print "File:",file,"Match for", pippo 
                delta =H5d2[pippo].attrs['delta']
                origin=H5d2[pippo].attrs['origin']
                sizeA =H5d2[pippo].shape
                d[var].append(np.array(H5d2[pippo]))
                tmpX=np.arange(0,sizeA[1])*delta[0]+origin[0]
                tmpY=np.arange(0,sizeA[0])*delta[1]+origin[1]
                tmpX_1,tmpY_1=np.meshgrid(tmpX, tmpY)
                d[PLANE[0]].append(tmpX_1)
                d[PLANE[1]].append(tmpY_1)
                found = True
        H5d2.close()
        if found:
            break
    return d
    
    
##################################################
# plot_1d_var                                    #
# plots a variable along an axys at a given time #
# "PATH" is the base simulation folder in a      #
# simfactory-like structure                      #
# "AXYS" is "x" or "y" or "z"                    #
# "time" is the time in ms                       #
# "rl" is the refinement level (0 -> coarser)    #
# "var" is the variable to be plotted            #
# "color" is the plot line color                 #
##################################################


def plot_1d_var(PATH='./',AXYS='x',time=0,rl=0,var='Bvec[2]',color='r'):
    
    x1 = AXYS
    if AXYS == 'x':
        PLANE = 'xy'
        x2 = 'y'
    elif AXYS == 'y':
        PLANE = 'yz'
        x2 = 'z'
    elif AXYS == 'z':
        PLANE = 'xz'
        x2 = 'x'
    else:
        print "Axys not recognized!"
    
    
    data = Import_HDF5_2d(PATH,PLANE,time,rl,var)
    for comp in range(len(data[var])):
        index = 0
        for i in data[x2][comp]:
            if all(abs(j) < 1e-10 for j in i):
                plt.plot(data[x1][comp][index],data[var][comp][index],color)
            index += 1
    plt.show()
        
##################################################
# plot_2d_var                                    #
# plots a pseudocolor 2d representation of var   #
# at the given time in the given plane           #
# "PATH" is the base simulation folder in a      #
# simfactory-like structure                      #
# "PLANE" is "xy" or "yz" or "xz"                #
# "time" is the time in ms                       #
# "rl" is the refinement level (0 -> coarser)    #
# "var" is the variable to be plotted            #
# "colormap" is the plot color map               #
##################################################


def plot_2d_var(PATH='./',PLANE='xy',time=0,rl=0,var='rho',colormap='RdBu'):
    
    data = Import_HDF5_2d(PATH,PLANE,time,rl,var)
    vmax=np.amax(data[var][0])
    vmin=np.amin(data[var][0])
    for component in data[var]:
        if vmax < np.amax(component):
            vmax=np.amax(component)
        if vmin > np.amin(component):
            vmin=np.amin(component)
    for comp in range(len(data[var])):
        plt.pcolormesh(data[PLANE[0]][comp],data[PLANE[1]][comp],data[var][comp],cmap=colormap,vmin=vmin,vmax=vmax)
    plt.colorbar()
    plt.show()


########
# TODO #
########

# Reading time limits for each restart to select more efficently 
# in which one are the requested data, instead of trying them all in order.

# Plot several pseudocolor with different times in a sigle figure 
# (like visit snapshots), 
# maybe rewriting Import_HDF5_2d to accept multiple time values.

# Plot several 1d lines in a single plot with different times. 

# Plot also some variables calculated in post-processing, like Omega

# Appropriate units conversions from CU to mks.

# Make movies!
 

